var app = angular.module('app', ['ngMaterial', 'ngMessages']);
var chat = angular.element(document.querySelector('.chat-int'));

app.controller('general', function($scope, $attrs, $element, $sce, $timeout) {
  var message = 'test';
  var items_1 = [{
      'id': 1,
      'ico': 'ico-men',
      'text': 'Lorem ipsum'
    },
    {
      'id': 2,
      'ico': 'ico-women',
      'text': 'Lorem ipsum'
    }
  ];
  var items_2 = [{
      'id': 21,
      'text': 'Lorem ipsum'
    },
    {
      'id': 22,
      'text': 'Lorem ipsum'
    },
    {
      'id': 23,
      'text': 'Lorem ipsum'
    },
    {
      'id': 24,
      'text': 'Lorem ipsum'
    },
    {
      'id': 25,
      'text': 'Lorem ipsum'
    },
    {
      'id': 26,
      'text': 'Lorem ipsum'
    },
    {
      'id': 27,
      'text': 'Lorem ipsum'
    }
  ];
  var items_3 = [{
      'id': 31,
      'ico': 'ico-passboard',
      'text': 'Lorem ipsum'
    },
    {
      'id': 32,
      'ico': 'ico-cap',
      'text': 'Lorem ipsum'
    },
    {
      'id': 33,
      'ico': 'ico-cart',
      'text': 'Lorem ipsum'
    },
    {
      'id': 34,
      'ico': 'ico-wallet',
      'text': 'Lorem ipsum'
    },
    {
      'id': 35,
      'ico': 'ico-brush',
      'text': 'Lorem ipsum'
    },
    {
      'id': 36,
      'ico': 'ico-dollar',
      'text': 'Lorem ipsum'
    },
    {
      'id': 37,
      'ico': 'ico-car-green',
      'text': 'Lorem ipsum'
    },
    {
      'id': 38,
      'ico': 'ico-question',
      'text': 'Lorem ipsum'
    }
  ];
  $scope.messages = [];
  $scope.myDate = new Date();
  $scope.CreateMessage = function() {
    //$scope.messages.push(message);
    //alert($scope.messege);
    //$scope.item = $sce.trustAsHtml('<div class="messege-person"><div class="messege"><span>'+$scope.messege+'</span></div></div>');
  }
  $scope.items_1 = items_1;
  $scope.items_2 = items_2;
  $scope.items_3 = items_3;
  $scope.isActive = true;
  $scope.FirstPart = true;
  $scope.SecondPart = false;
  $scope.TreePart = false;
  $scope.HideAll = false;
  $scope.FinalPart = false;
  $scope.select = function(index) {
    $scope.selected = index;
    $timeout(function() {
      $scope.isActive = false;
      $scope.SecondPart = true;
    }, 1000);
  };
  $scope.select2 = function(index) {
    $scope.selected2 = index;
    $scope.TreePart = true;
    $timeout(function() {
      $scope.FirstPart = false;
      $scope.SecondPart = false;
    }, 500);
  };
  $scope.select3 = function(index) {
    $scope.selected3 = index;
    $scope.HideAll = true;
    $timeout(function() {
      $scope.FinalPart = true;
    }, 200);
  };

});

app.config(function($mdDateLocaleProvider) {
  $mdDateLocaleProvider.months = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
    'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'
  ];
  $mdDateLocaleProvider.shortMonths = ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun',
    'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'
  ];
  $mdDateLocaleProvider.days = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sábado'];
  $mdDateLocaleProvider.shortDays = ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'];
  $mdDateLocaleProvider.firstDayOfWeek = 0;
  $mdDateLocaleProvider.weekNumberFormatter = function(weekNumber) {
    return 'Semana ' + weekNumber;
  };
  $mdDateLocaleProvider.msgCalendar = 'Calendario';
  $mdDateLocaleProvider.msgOpenCalendar = 'Abrir calendario';
});

app.directive('ngEnter', function() {
  return function(scope, element, attrs) {

    element.bind("keydown keypress", function(event) {
      if (event.which === 13) {
        scope.$apply(function() {
          scope.$eval(attrs.ngEnter);
        });

        event.preventDefault();
      }
    });
  };
});


angular.element(document).ready(function() {

  angular.bootstrap(document, ['app']);

});
