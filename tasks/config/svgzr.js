module.exports = function(grunt) {
  grunt.config.set('svgzr', {
    dist: {
      options: {
        templateFileSvg: './tasks/templates/svg_to_sass_sass.mst',
        files: {
          cwdSvg: 'resources/source/svgs/'
        },
        prefix: '',
        encodeType: 'base64',
        svg: {
          destFile: 'resources/source/sass/_icons.scss'
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-svgzr');
};
