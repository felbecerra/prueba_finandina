module.exports = function (grunt) {
  grunt.config.set('htmlmin', {
   dist: {                                      // Target
      options: {                                 // Target options
        removeComments: true,
        collapseWhitespace: true
      },
      files: {                                   // Dictionary of files
        'index.html': 'resources/views/html/index.html',
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-htmlmin');
};
