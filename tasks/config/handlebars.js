module.exports = function(grunt) {
  grunt.config.set('handlebars', {
    compile: {
      options: {
        namespace: 'Handlebars.templates',
        processName: function(filePath) {
          return filePath.replace(/resources\/source\/handlebars\/(.*)(.handlebars)/, '$1').split('/').join('_').toLowerCase();
        }
      },
      files: {
        '.js-cache/app.templates.js': [ 'resources/source/handlebars/**/*.handlebars' ]
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-handlebars');
};