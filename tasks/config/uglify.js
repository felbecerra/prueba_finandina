module.exports = function(grunt) {
  grunt.config.set('uglify', {
    templates: {
      options: { sourceMap: false },
      src: '.js-cache/app.templates.js',
      dest: 'resources/assets/js/app/app.templates-min.js'
    },
    core: {
      options: {
        sourceMap: true,
        drop_console: false
      },
      files: {
        'resources/assets/js/app/app.core-min.js': ['.js-cache/core_and_extend.js'],
        'resources/assets/js/app/app.app-min.js': ['resources/source/js/*.js']
      }
    },
    app: {
      options: {
        sourceMap: true,
        drop_console: false
      },
      files: {
        'resources/assets/js/app/app.app-min.js': ['resources/source/js/*.js']
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-uglify');
};