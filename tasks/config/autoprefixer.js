module.exports = function (grunt) {
  grunt.config.set('autoprefixer', { 
    dist:{
      files:{
        'resources/assets/css/style.css':'resources/assets/css/style.css'
      }
    }
  });

  grunt.loadNpmTasks('grunt-autoprefixer');
};
